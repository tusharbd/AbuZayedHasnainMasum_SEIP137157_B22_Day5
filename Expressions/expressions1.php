<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 10:52 AM
 */
######################################################################
echo "<h2><br>Source Code of " . basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of " . basename((string)__FILE__) . "<hr></h2>";
#######################################################################

function MakeItDouble($i)
{
    return $i*2;
}

$a=$b=$c=$d=$e=$f=$g=$h=0;
echo ' $a= '.$a.', $b= '.$b.', $c= ' .$c . ', $d= '.$d.', $e= '.$e.', $f= ' .$f . ', $g='.$g.', $h='.$h. ".<br>";
$b = $a = 5;        /* assign the value five into the variable $a and $b */
echo ' $a= '.$a.', $b= '.$b.', $c= ' .$c . ', $d= '.$d.', $e= '.$e.', $f= ' .$f . ', $g='.$g.', $h='.$h. ".<br>";
$c = $a++;          // post-increment, assign original value of $a (5) to $c
echo ' $a= '.$a.', $b= '.$b.', $c= ' .$c . ', $d= '.$d.', $e= '.$e.', $f= ' .$f . ', $g='.$g.', $h='.$h. ".<br>";
$e = $d = ++$b;     // pre-increment, assign the incremented value of at this point, both $d and $e are equal to 6
echo ' $a= '.$a.', $b= '.$b.', $c= ' .$c . ', $d= '.$d.', $e= '.$e.', $f= ' .$f . ', $g='.$g.', $h='.$h. ".<br>";
$g = MakeItDouble(++$e);  // assign twice the value of $e after the increment, 2*7 = 14 to $g
echo ' $a= '.$a.', $b= '.$b.', $c= ' .$c . ', $d= '.$d.', $e= '.$e.', $f= ' .$f . ', $g='.$g.', $h='.$h. ".<br>";
$h = $g += 10;      // first, $g is incremented by 10 and ends with the  value of 24. the value of the assignment (24)
                    // is then assigned into $h, and $h ends with the value of 24 as well.
echo ' $a= '.$a.', $b= '.$b.', $c= ' .$c . ', $d= '.$d.', $e= '.$e.', $f= ' .$f . ', $g='.$g.', $h='.$h. ".<br>";


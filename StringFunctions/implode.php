<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 5:38 PM
 */
######################################################################
echo "<h2><br>Source Code of ".basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of ".basename((string)__FILE__) . "<hr></h2>";
#######################################################################


$str = "Hello world. It's a beautiful day.";
$myExplodedStr = explode(" ",$str);
print_r ($myExplodedStr);

echo '<br> Let us bring the string back by implode (" ",$myExplodedStr); <br>';
echo implode (" ",$myExplodedStr);

?>
<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 2:18 PM
 */
######################################################################
echo "<h2><br>Source Code of " . basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of " . basename((string)__FILE__) . "<hr></h2>";
#######################################################################

$x = rand(10, 100)    ;
$y = rand(10, 100)    ;

echo '<strong> When $x = '. $x. ' and $y = '. $y . '</strong><br>';

if($x == $y ) echo '==	Equal	$x == $y	Returns true if $x is equal to $y<br>';
if($x === $y )echo '=== Identical	$x === $y	Returns true if $x is equal to $y, and they are of the same type<br>';
if($x != $y)  echo '$x != $y	Returns true if $x is not equal to $y <br>';
if($x <> $y) echo '<>	Not equal	$x <> $y	Returns true if $x is not equal to $y<br>';
if($x !== $y) echo '!== Not identical	$x !== $y Returns true if $x is not equal to $y, or they are not of the same type<br>';
if($x > $y) echo '>	Greater than	$x > $y	Returns true if $x is greater than $y<br>';
if($x < $y) echo '<	Less than	$x < $y	Returns true if $x is less than $y<br>';
if($x >= $y) echo '>= Greater than or equal to $x >= $y  Returns true if $x is greater than or equal to $y<br>';
if($x <= $y) echo '<=	Less than or equal to	$x <= $y	Returns true if $x is less than or equal to $y<br>';
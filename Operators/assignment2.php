<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 1:51 PM
 */
######################################################################
echo "<h2><br>Source Code of " . basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of " . basename((string)__FILE__) . "<hr></h2>";
#######################################################################

$x = 10 + rand(1,100);
$y = rand(1,10);
$z = $x + $y;
echo 'Initially: $x ='.$x . ', $y ='. $y. ' and $z ='.$z.'<br>';

$z  +=  ($x + $y) ;

echo '<br>Implementing Example#2 of Assignment Operator:  <strong> $z  +=  ($x + $y);</strong> <br>';
echo '<br>Finally: $x ='.$x . ', $y ='. $y. ' and $z ='.$z;
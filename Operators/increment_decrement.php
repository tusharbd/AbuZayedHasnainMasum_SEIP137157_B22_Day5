<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 2:44 PM
 */
######################################################################
echo "<h2><br>Source Code of " . basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of " . basename((string)__FILE__) . "<hr></h2>";
#######################################################################
$x = rand(1,100);
$y = rand(1, 100);

echo 'Initial Value of $x = '.$x.' and $y = '.$y.'<br>';
$z = (++$x) + ($y++);
echo 'Test1 $z = (++$x) + ($y++) = ' .$z. '<br>' ;

echo 'Latest Value of $x = '.$x.' and $y = '.$y.'<br>';
$z = (--$x) + ($y--);
echo 'Test2 $z = (--$x) + ($y--) = ' .$z .'<br>';

echo 'Latest Value of $x = '.$x.' and $y = '.$y.'<br>';
$z = (++$x) - ($y++);
echo 'Test3 $z = (++$x) - ($y++) = ' .$z. '<br>' ;

echo 'Latest Value of $x = '.$x.' and $y = '.$y.'<br>';
$z = (--$x) - ($y--);
echo 'Test4 $z = (--$x) - ($y--) = ' .$z .'<br>';
<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 1:28 PM
 */
######################################################################
echo "<h2><br>Source Code of " . basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of " . basename((string)__FILE__) . "<hr></h2>";
#######################################################################

$x = 10 + rand(1,100);
$y = rand(1,10);

$z = $x + $y;


echo 'Example#1 of Assignment Operator:   $z =  ($x + $y); <br>';
echo '$x ='.$x . ', $y ='. $y. ' and $z ='.$z;
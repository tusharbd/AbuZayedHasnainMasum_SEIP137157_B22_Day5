<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 3:29 PM
 */
######################################################################
echo "<h2><br>Source Code of " . basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of " . basename((string)__FILE__) . "<hr></h2>";
#######################################################################
$x = TRUE;
$y = FALSE;
echo 'Initial Value of $x = TRUE and $y = FALSE then <br><br>';

echo '<br> ($x & $y) is ';
if($x & $y )  echo 'TRUE  ,';
else  echo 'FALSE  ,';
echo ' ($x && $y) is ';
if($x && $y )  echo 'TRUE  ,';
else  echo 'FALSE  ,';
echo ' ($x And $y) is ';
if($x And $y )  echo 'TRUE.';
else  echo 'FALSE.';

echo '<br>  ($x | $y)  is ';
if($x | $y )  echo 'TRUE  ,';
else  echo 'FALSE  ,';
echo ' ($x || $y) is ';
if($x || $y )  echo 'TRUE  ,';
else  echo 'FALSE  ,';
echo ' ($x Or $y) is ';
if($x Or $y )  echo 'TRUE .  ';
else  echo 'FALSE. ';

echo '<br> (!$x) is ';
if(!$x )  echo 'TRUE.';
else  echo 'FALSE.';

echo '<br>  ($x Xor $y) is ';
if($x Xor $y )  echo 'TRUE .  ';
else  echo 'FALSE. ';



<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 4:22 PM
 */
######################################################################
echo "<h2><br>Source Code of " . basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of " . basename((string)__FILE__) . "<hr></h2>";
#######################################################################



// Example 1
$a = 3 * 3 % 5; // (3 * 3) % 5 = 4
// ternary operator associativity differs from C/C++
$a = true ? 0 : true ? 1 : 2; // (true ? 0 : true) ? 1 : 2 = 2

$a = 1;
$b = 2;
$a = $b += 3; // $a = ($b += 3) -> $a = 5, $b = 5
echo "Example1 Output: <br>";
echo 'The value of $a = ' . $a ."<br><br>";


// Example 2
echo "Example2 Output: ";

$a = 1;
echo $a + $a++; // may print either 2 or 3

$i = 1;
$array[$i] = $i++; // may set either index 1 or 2
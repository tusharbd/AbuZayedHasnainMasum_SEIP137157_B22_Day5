<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 12:48 AM
 */
######################################################################
echo "<h2><br>Source Code of ".basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of ".basename((string)__FILE__) . "<hr></h2>";
#######################################################################

echo '<strong>Current Directory (__DIR__):  </strong> <br><br>'.__DIR__."<hr>";

$files = scandir((string)(__DIR__),2);
echo '<br> <strong> List of files  in the current directory: </strong>';

while ( list($key, $val) = each($files)){
    echo "$val <br> ";
} // end of while



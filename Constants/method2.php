<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 5:00 AM
 */
######################################################################
echo "<h2><br>Source Code of ".basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of ".basename((string)__FILE__) . "<hr></h2>";
#######################################################################

$ValueOf__METHOD__= "";
$ValueOf__FUNCTION__="";

class JustAnotherClass{

    public function PublicMethod(){
        $GLOBALS['ValueOf__METHOD__'] = __METHOD__;
        $GLOBALS['ValueOf__FUNCTION__'] = __FUNCTION__;
    }//end of PublicMethod()
}//end of JustAnotherClass

$MyObject = new JustAnotherClass();
$MyObject->PublicMethod();

echo "__METHOD__   output: <strong>". $ValueOf__METHOD__. "</strong><br>";

echo "__FUNCTION__ output: <strong>". $ValueOf__FUNCTION__. "</strong> <br>";
<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 4:08 AM
 */
######################################################################
echo "<h2><br>Source Code of ".basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of ".basename((string)__FILE__) . "<hr></h2>";
#######################################################################




function UserDefineFunction(){
    return "<br> __FUNCTION__ value inside the UserDefineFunction() is : [ ". __FUNCTION__." ]<br>";
}//end of

echo UserDefineFunction();

echo "<br> __FUNCTION__ value outside the UserDefineFunction() is : [ ". __FUNCTION__." ]<br>";

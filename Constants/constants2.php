<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/18/2016
 * Time: 8:49 PM
 */
######################################################################
echo "<h2><br>Source Code of ".basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of ".basename((string)__FILE__) . "<hr></h2>";
#######################################################################

define("PI",pi());

echo "As we know the value of the constant PI is = ".PI." and Area of a Circle = (PI * Diameter) <br> 
      then we can say:<hr>";

for($Diameter=1;$Diameter<=25;$Diameter++){

    if($Diameter%10==0)
        echo"<hr>";
    
    echo "When Diameter is $Diameter cm then the area of the circle is = " . (string)(PI * $Diameter) . " cm².<br>";

}//end of for





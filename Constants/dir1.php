<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 12:29 AM
 */
######################################################################
echo "<h2><br>Source Code of ".basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of ".basename((string)__FILE__) . "<hr></h2>";
#######################################################################


echo "The current directory is : <br><br> <strong>__DIR__ output: </strong>". __DIR__ ."<hr>";

echo "<strong>getcwd() output: </strong>".getcwd() . "\n";


echo '<hr>So we have found there is no difference in output between __DIR__ magic constant and  getcwd() function.';
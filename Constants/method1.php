<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 4:37 AM
 */

######################################################################
echo "<h2><br>Source Code of ".basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of ".basename((string)__FILE__) . "<hr></h2>";
#######################################################################



class MagicConstantTestingClass{

    public function TestMethod(){
        return (string) __METHOD__;
    }
}

$MagicTestObject =  new MagicConstantTestingClass();
echo $MagicTestObject->TestMethod();
echo "<br>";

$NewObject = new MagicConstantTestingClass();
echo $NewObject->TestMethod();

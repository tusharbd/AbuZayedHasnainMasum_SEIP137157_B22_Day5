<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 5:39 AM
 */
namespace MyProject1;


######################################################################
echo "<h2><br>Source Code of " . basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of " . basename((string)__FILE__) . "<hr></h2>";
#######################################################################
echo "The name of this namespace is ".__NAMESPACE__."<br>";


namespace MyProject2;
echo "The name of this namespace is ".__NAMESPACE__."<br>";

namespace MyProject3;
echo "The name of this namespace is ".__NAMESPACE__;


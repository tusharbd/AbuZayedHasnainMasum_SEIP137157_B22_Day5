<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 3:22 AM
 */
######################################################################
echo "<h2><br>Source Code of ".basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of ".basename((string)__FILE__) . "<hr></h2>";
#######################################################################


$FunctionName="";

function MyFunctionForAddition( $a, $b){
    $GLOBALS['FunctionName']=  __FUNCTION__;
    return ($a + $b);
}  // end of MyFunctionForAddition

$a = rand(1,100);
$b = rand(1,100);

echo "Sum of ".$a ."+". $b." = ". (string) MyFunctionForAddition($a,$b)."<br>";

echo $FunctionName ."() call was successful!";
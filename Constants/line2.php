<?php
/**
 * Created by PhpStorm.
 * User: Abu Zayed Hasnain Masum, SEIP-137157
 * Date: 5/19/2016
 * Time: 4:28 AM
 */
######################################################################
echo "<h2><br>Source Code of ".basename((string)__FILE__) . "</h2><hr>";
show_source(__FILE__);
echo "<hr>";
echo "<h2>Output of ".basename((string)__FILE__) . "<hr></h2>";
#######################################################################


echo '<strong>Let\'s see the impact of __LINE__ inside a for loop: </strong><br>';

for($i = 1; $i<=5; $i++){
    echo "Executing Line# ".__LINE__;
    echo "<br>";
}